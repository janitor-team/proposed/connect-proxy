connect-proxy (1.105-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Bump debhelper from deprecated 9 to 12.
  * Remove connect-proxy.1 from git repo, since we can generate it when
    building.
  * debian/control:
    - Remove Philippe Coval from Maintainer, since email is not
      reachable (Closes: #978623).

 -- Roger Shimizu <rosh@debian.org>  Thu, 31 Dec 2020 20:47:54 +0900

connect-proxy (1.105-1.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Roger Shimizu ]
  * debian/control:
    - Create git repo on salsa, and add Vcs-* URL.
    - Add Rules-Requires-Root: no
  * debian/copyright:
    - Use secure copyright file specification URI.
    - Remove Makefile entry, which does not exist.
  * debian/rules:
    - Add security hardening.

  [ Helmut Grohne ]
  * Fix FTCBFS: (Closes: #901453)
    + Let dpkg's buildtools.mk set up $(CC).
    + Don't hard code gcc.

 -- Roger Shimizu <rosh@debian.org>  Thu, 17 Dec 2020 02:19:57 +0900

connect-proxy (1.105-1) unstable; urgency=medium

  * New upstream release (rebuilt tarball)
  * Removed quilt, created a Makefile, switched to dh7
  * Fix non-comformant date in debian/changelog (Closes: #830423)
  * Removed patch (Closes: #689051)

 -- Christian Bayle <bayle@debian.org>  Mon, 31 Oct 2016 21:03:15 +0100

connect-proxy (1.101-1) unstable; urgency=low

  * New upstream release (rebuilt tarball)
  * debian/rules :  migrated to quilt
  * debian/control : bump standards
  * debian/* : updadated maintainer email, and upstream url
  * fixed man page

 -- Philippe Coval <rzr@gna.org>  Fri, 11 Dec 2009 12:36:01 +0100

connect-proxy (1.100-1) unstable; urgency=low

  * New upstream release (100 svn revision)
  * FIX: strip (Closes: #436660)
  * ADD: manpage link (Closes: #443342)

 -- Philippe Coval <rzr@users.sf.net>  Wed, 20 Sep 2007 23:59:42 +0200

connect-proxy (1.96-1) unstable; urgency=low

  * New upstream release

 -- Philippe Coval <rzr@users.sf.net>  Sat,  6 May 2006 00:59:39 +0200

connect-proxy (1.95-3) unstable; urgency=low

  * Re-uploaded with all the changelog so the ITP are correctly closed

 -- Christian Bayle <bayle@debian.org>  Sat,  1 Oct 2005 23:59:05 +0200

connect-proxy (1.95-2) unstable; urgency=low

  * more detailed example in manpage

 -- Philippe Coval <rzr@users.sf.net>  Sat, 24 Sep 2005 17:56:20 +0200

connect-proxy (1.95-1) unstable; urgency=low

  * New upstream release

 -- Philippe Coval <rzr@users.sf.net>  Wed,  7 Sep 2005 22:17:43 +0200

connect-proxy (1.93-1) unstable; urgency=low

  * Initial Release (renamed connect-proxy) Closes: #306226
  * Closes: #294943.

 -- Philippe Coval <rzr@users.sf.net>  Fri, 22 Apr 2005 22:58:56 +0200
